import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  snake = {
    direction: 40,
    x: 3,
    y: 5
  }

  gridSize: number = 16;
  gameGrid = [];

  @HostListener('document:keydown', ['$event'])
  handleKeyBoardEvents(event: KeyboardEvent) {
    // //go left
    // if (event.keyCode === 37) {
    //   this.snake.x--;
    // }
    // //go right
    // if (event.keyCode === 39) {
    //   this.snake.x++;
    // }
    // //go up
    // if (event.keyCode === 40) {
    //   this.snake.y++;
    // }
    // //go down
    // if (event.keyCode === 38) {
    //   this.snake.y--;
    // }
    this.snake.direction = event.keyCode;
  }

  constructor() { }

  ngOnInit() {
    this.initGrid();
    setInterval(() => {
      this.moveSnake()
    }, 200);
  }

  initGrid() {
    this.gameGrid = [];

    for (let i = 0; i < this.gridSize; i++) {
      this.gameGrid[i] = [];
      for (let j = 0; j < this.gridSize; j++) {
        this.gameGrid[i][j] = true;
      }
    }
  }

  isSnake(y: number, x: number): Boolean {
    return (x === this.snake.x && y === this.snake.y)
  }

  moveSnake() {
    if (this.snake.direction === 37) {
      if (this.snake.x === 0){
        this.snake.x = this.gameGrid.length - 1;
      } else {
        this.snake.x--;
      }
    }
    if (this.snake.direction === 39) {
      if (this.snake.x === this.gameGrid.length -1){
        this.snake.x = 0;
      } else {
        this.snake.x++;
      }
    }
    if (this.snake.direction === 38) {
      if (this.snake.y === 0){
        this.snake.y = this.gameGrid.length-1
      } else {
        this.snake.y--;
      }
    }
    if (this.snake.direction === 40) {
      if (this.snake.y === this.gameGrid.length -1){
        this.snake.y = 0;
      } else {
        this.snake.y++;
      }
    }
  }

}
