window.onload = function (){

    let lienHomme = document.getElementById('lien-homme');
    let lienFemme = document.getElementById('lien-femme');
    let lienEnfant = document.getElementById('lien-enfant');

    let menuHomme = document.getElementById('menu-homme');
    let menuFemme = document.getElementById('menu-femme');
    let menuEnfant = document.getElementById('menu-enfant');

    lienHomme.addEventListener('mouseover', () => {
        menuHomme.style.display = "block";
    });

    lienFemme.addEventListener('mouseover', () => {
        menuFemme.style.display = "block";
    });

    lienEnfant.addEventListener('mouseover', () => {
        menuEnfant.style.display = "block";
    });

    lienHomme.addEventListener('mouseleave', () => {
        menuHomme.style.display = "none";
    });

    lienFemme.addEventListener('mouseleave', () => {
        menuFemme.style.display = "none";
    });

    lienEnfant.addEventListener('mouseleave', () => {
        menuEnfant.style.display = "none";
    });
}