import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-keyboard',
  templateUrl: './keyboard.component.html',
  styleUrls: ['./keyboard.component.css']
})
export class KeyboardComponent implements OnInit {
  
  grid: Array<number>;
  password: String = "";
  realPassword: String = "";
  constructor() { }

  ngOnInit() {
    this.grid = this.randArray();
  }

  clic(event: any) {
    this.realPassword += event.target.innerText;
    if(event.target.innerText){
      this.password += "" + Math.floor(Math.random());
    }
  }

  randArray(): Array<number> {
    let randNums: Array<number> = [, , , , , 0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    for (let i = randNums.length; i > 0; i--){
      const j = Math.floor(Math.random() * (i+1));
      [randNums[i] , randNums[j]] = [randNums[j], randNums[i]];
    }
    return randNums;
  }

  keydown(): Boolean{
    return false;
  }

  clear(): void {
    this.password = "";
    this.realPassword = "";
  }

  correct(): void{
    this.password = this.password.substring(0, this.password.length - 1);
    this.realPassword = this.realPassword.substring(0, this.realPassword.length - 1);
  }

}
