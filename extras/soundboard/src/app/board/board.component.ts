import { Component, OnInit} from '@angular/core';
import { NOTES, NAMES } from './notes';




@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css'],
})
export class BoardComponent implements OnInit {
  
  //get note names and values
  notes = NOTES;
  names = NAMES;

  //allows ngmodel to map label and input together
  boardNote:number[] = Array(72).fill(false);

  //audio params
  audioContext: AudioContext = new AudioContext();;
  oscillatorNode: OscillatorNode;
  gainNode: GainNode;


  constructor() { }
  ngOnInit() {
  }

  playSound(frequency: number = 440.0){
    this.oscillatorNode = this.audioContext.createOscillator();
    this.gainNode = this.audioContext.createGain();

    this.oscillatorNode.frequency.value = frequency;
    
    this.oscillatorNode.type = "sine";

    this.oscillatorNode.connect(this.gainNode);
    this.gainNode.connect(this.audioContext.destination);
    this.oscillatorNode.start();
  }

  stopSound() {
    this.gainNode.gain.setTargetAtTime(this.gainNode.gain.value, this.audioContext.currentTime, 0.015);
    this.gainNode.gain.exponentialRampToValueAtTime(0.0001, this.audioContext.currentTime + 0.5);
  }

  playNote(frequency: number = 440.0){
    this.playSound(frequency);
    this.stopSound();
  }

  playSequence(){
    for (let i = 0; i < this.boardNote.length; i++){
      ((i) => {
        setTimeout(()=>{
          if (this.boardNote[i]){
            console.log(this.notes[i][1]);
            this.playNote(+this.notes[i][1]);
          }
        },50*i);
      })(i);
    }
  }
}