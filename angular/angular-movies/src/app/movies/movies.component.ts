import { Component, OnInit } from '@angular/core';
import { Movie } from '../movie';
import { MovieService } from '../movie.service';

@Component({
    selector: 'app-movies',
    templateUrl: './movies.component.html',
    styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

    movies: Movie[];

    constructor(private movieService: MovieService) { }

    ngOnInit() {
        this.getMovies();
    }

    getMovies(): void {
        this.movieService.getMovies()
            .subscribe(movies => this.movies = movies);
    }

    add(title: string, year: string): void {
        title = title.trim();
        year = year.trim();
        if (!title || !year) { return; }
        this.movieService.addMovie( {title: title, year: parseInt(year)} as Movie )
        .subscribe(movie => {
            this.movies.push(movie)
        });
    }

    delete(movie: Movie): void {
        this.movieService.deleteMovie(movie).subscribe(
            () => {
                this.movies = this.movies.filter(deletedMovie => deletedMovie !== movie)
            }
        );
    }
}
