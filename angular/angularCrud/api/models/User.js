const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let User = new Schema({
    userName : {
        type: String
    },
    userEmail: {
        type: String
    }, 
    userPhone: {
        type: String
    }
}, {
    collection: 'user'
});

module.exports = mongoose.model('User', User);