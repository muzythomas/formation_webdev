const express = require('express'),
path = require('path'),
bodyParser = require('body-parser'),
cors = require('cors'),
mongoose = require('mongoose'),
dbConfig = require('./db');

//connection à mongodb
mongoose.Promise = global.Promise;
mongoose.connect(dbConfig.DB, {useNewUrlParser: true}).then(
    () => {console.log('Database connected')},
    err => {console.log(`Error connecting to the database: ${err}` )}
);

const userRoute = require('./routes/user.route');

//création de l'application server
const app = express();
app.use(bodyParser.json());
app.use(cors());
app.use('/user', userRoute);
//définition du port de communication
let port = process.env.PORT || 4000;

//lancement du server
const server = app.listen(port, () => {
    console.log(`Listening on port ${port}`)
})