const express = require('express');
const userRoutes = express.Router();

let User = require('../models/User');

userRoutes.route('/add').post((req, res) => {
    let user = new User(req.body);
    user.save()
        .then(user => {
            res.status(200).json({'user': `user ${user.id} added successfully`});
        })
        .catch(err => {
            res.status(400).send('userAdd: unable to save to database')
        });
});

userRoutes.route('/').get((req, res) => {
    User.find((err, users) => {
        if(err){
            console.error(err);
        }
        else {
            res.json(users)
        }
    });
});

userRoutes.route('/edit/:id').get((req, res) => {
    let id = req.params.id;
    User.findById(id, (err, user)=>{
        if(err){
            console.error(err);
        }else {
            res.json(user)
        }
    });
});

userRoutes.route('/update/:id').post((req, res) => {
    let id = req.params.id;
    User.findById(id, (err, next, user) => {
        if (!user){
            return next(new Error('could not load Document'));
        } else {
            user.userName = req.body.userName;
            user.userEmail = req.body.userEmail;
            user.userPhone = req.body.userPhone;
            user.save().then(user => {
                res.json('Update Complete');
            })
            .catch(err => {
                res.status(400).send(`unable to update database : ${id}`)
            });
        }
    });
});

userRoutes.route('/delete/:id').delete((req, res) => {
    let id = req.params.id;
    User.findByIdAndRemove({_id: id}, (err, user) => {
        if(err){
            res.json(err);
        } else {
            res.json(`User ${id} successfully removed`);
        }
    });
});

module.exports = userRoutes;