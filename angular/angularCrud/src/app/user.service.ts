import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
    providedIn: 'root'
})
export class UserService {

    uri = 'http://localhost:4000/user';
    constructor(private http: HttpClient) { }

    addUser(userName: string, userEmail: string, userPhone: string):void{
        const user = {
            userName: userName,
            userEmail: userEmail,
            userPhone: userPhone
        };
        console.log(user);
        this.http.post(`${this.uri}/add`, user)
            .subscribe(res => console.log('addUser: Done'));
    }

    getUsers() {
        return this.http.get(`${this.uri}`);
    }

    deleteUser(id: string){
        return this.http.delete(`${this.uri}/delete/${id}`);
    }

}
