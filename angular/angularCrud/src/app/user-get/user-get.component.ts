import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import User from '../User';
import { Location } from '@angular/common';


@Component({
  selector: 'app-user-get',
  templateUrl: './user-get.component.html',
  styleUrls: ['./user-get.component.css']
})
export class UserGetComponent implements OnInit {

  users: User[];
  constructor(private userService: UserService, private location: Location) { }
  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.userService.getUsers().subscribe(
      (data: User[]) => {
        this.users = data;
      }
    )
  };

  deleteUser(id) {
    this.userService.deleteUser(id).subscribe(
      () => {
        location.reload();
      }
    );
    
  }
}
