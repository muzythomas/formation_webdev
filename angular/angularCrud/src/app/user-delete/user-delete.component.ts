import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-delete',
  templateUrl: './user-delete.component.html',
  styleUrls: ['./user-delete.component.css']
})
export class UserDeleteComponent implements OnInit {

  id: string;
  constructor(private router: Router, private route: ActivatedRoute, private userService: UserService) { }

  ngOnInit() {
    this.id = this.route.paramMap['id'];
    this.deleteUser(this.id);
    this.router.navigate(['./user'])
  }

  deleteUser(id){
    this.userService.deleteUser(id);
    console.log('coucou');
  }

}
