export default class User {
    userName: string;
    userEmail: string;
    userPhone: string;
}