import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../user.service';


@Component({
    selector: 'app-user-add',
    templateUrl: './user-add.component.html',
    styleUrls: ['./user-add.component.css']
})
export class UserAddComponent implements OnInit {

    formGroup: FormGroup;
    constructor(private userService: UserService, private formBuilder: FormBuilder) { }

    ngOnInit() {
        this.createForm();
    }

    createForm(): void {
        this.formGroup = this.formBuilder.group({
            userName: ['', Validators.required],
            userEmail: ['', [Validators.required, Validators.email]],
            userPhone: ['', [Validators.required, Validators.pattern(
                //phone number pattern
                '^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\.0-9]*$'
            )]]
        }
        )
    }

    addUser(userName, userEmail, userPhone): void {
        this.userService.addUser(userName, userEmail, userPhone);
    }

}
