/*
*Exercice 2*: Coder une Course De Voitures

Pour ce faire, il faut suivre les conditions suivantes

• Créer une classe `Voiture` qui contiendra :

1. Une écurie (une chaine de caractères)

2. Une place dans la course (un nombre)

On instanciera cette classe en appellant `new Voiture(ecurie)` (ex: `const voiture1 = new Voiture("Mazda");`)

• Créer une classe `Course` qui contiendra :

1. Une liste de voitures (un tableau d'objets `Voiture`)

On intanciera cette classe en appellant `new Course(voitures)` (ex: `const voitures = [voiture1, voiture 2...]; const course = new Course(voitures);`) 
Il faudra pouvoir faire en sorte que les voitures puissent se dépasser entre elles, se faire éliminer (accident), et que le classement de la course puisse être affiché à tout moment.
*/

const ECURIE = {
    ASTON : "Aston Martin-Red Bull",
    BMW : "BMW Sauber F1 Team", 
    FERRARI : "Scuderia Ferrari",
    HAAS : "Haas F1 Team",
    MCLAREN : "McLaren Racing",
    MERCEDES : "Mercedes Grand Prix",
    RENAULT : "Renault Sport",
}

class Voiture{
    constructor(ecurie, place = 0){
        this.ecurie = ecurie;
        this.place = place;
    }
}

class Course{
    constructor(voitures, voitures_eliminees = []){
        this.voitures = voitures;
        this.voitures_eliminees = voitures_eliminees;
        this.organiser();
    }

    //réorganise le classement des voitures en leur attribuant la bonne place
    organiser(){
        //on vérifie qu'on ait bien un tableau
        if (Array.isArray(this.voitures)){
            for (let i = 0; i < this.voitures.length; i++){
                //on vérifie que ce soit bien une voiture
                if (this.voitures[i] instanceof Voiture){
                        this.voitures[i].place = i+1;             
                } else {
                    console.error(`erreur, ${this.voitures[i]} n'est pas une Voiture valide`);
                }
            }
        } else {
            console.error(`erreur, course invalide` );
        }
    }

    classement(){
        let classement = "Classement de la course :\n";
        for (const voiture of this.voitures){
            classement += `${voiture.place}: ${voiture.ecurie}\n`;
        }

        //si on a des voitures éliminées
        if (this.voitures_eliminees.length > 0){
            //on les affiche
            classement += "\nVoitures éliminées :\n"
            for (const elim of this.voitures_eliminees) {
                classement += `${elim.ecurie}\n`;
            }
        }
        return classement;
    }
    
    annonce_depassement(place1, place2){
        console.log(`${this.voitures[place1-1].ecurie} double ${this.voitures[place2-1].ecurie}!`)
    }

    //change les places de deux voitures a partir d'indices
    changer_place(place1, place2){
        this.annonce_depassement(place1, place2);

        let tmp = this.voitures[place1-1];
        this.voitures[place1-1] = this.voitures[place2-1] 
        this.voitures[place2-1] = tmp;

        this.organiser();
    }

    changer_place_voiture(voiture1, voiture2){
        this.annonce_depassement(voiture1.place, voiture2.place);

        let tmp = this.voitures[voiture1.place-1];
        this.voitures[voiture1.place-1] = this.voitures[voiture2.place-1] 
        this.voitures[voiture2.place-1] = tmp;

        this.organiser();
    }

    annonce_elimination(place1){
        console.log(`${this.voitures[place1 - 1].ecurie} est éliminée !`);
    }

    //elimine une voiture a partir d'un indice
    eliminer(place){
        this.annonce_elimination(place);
        this.voitures_eliminees.push(this.voitures[place]);
        this.voitures.splice(place-1, 1);
        this.organiser();
    }

    //elimine une voiture à partir d'un objet
    eliminer_voiture(voiture){
        this.annonce_elimination(voiture.place);
        this.voitures_eliminees.push(voiture);
        this.voitures.splice(voiture.place-1, 1);
        this.organiser();
    }

    depassement(voiture){
        if (this.voitures.includes(voiture)){
            if (voiture.place !== 1){
                let voiture_a_doubler = this.voitures[voiture.place - 2];
                this.changer_place_voiture(voiture, voiture_a_doubler);
            } else {
                console.error("Cette voiture est déjà première");
            }
        } else {
            console.error(`Cette voiture n'est pas dans la course`)
        }
    }

    depassement_place(place){
        if (this.voitures.includes(this.voitures[place-1])){
            if (place !== 1){
                this.changer_place(place, place -1);
            } else {
                console.error("Cette voiture est déjà première");
            }
        } else {
            console.error(`Cette voiture n'est pas dans la course`)
        }
    }
}

// on crée nos voitures
const aston = new Voiture(ECURIE.ASTON);
const bmw = new Voiture(ECURIE.BMW);
const ferrari =  new Voiture(ECURIE.FERRARI);
const haas = new Voiture(ECURIE.HAAS);
const mclaren = new Voiture(ECURIE.MCLAREN);
const mercedes = new Voiture(ECURIE.MERCEDES);
const renault = new Voiture(ECURIE.RENAULT);

//et on les ajoute a notre course via un tableau
const voitures_course = [aston, bmw, ferrari, haas, mclaren, mercedes, renault];
const course =  new Course(voitures_course);

console.log(course.classement());

//la 2ème voiture dépasse la première 
course.depassement_place(2);

console.log(course.classement());

course.depassement(aston);

console.log(course.classement());

course.depassement_place(1);
console.log(course.classement());

course.eliminer_voiture(bmw)
console.log(course.classement());

course.depassement(renault);
course.depassement(renault);
console.log(course.classement());