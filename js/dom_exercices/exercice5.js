let images = [
    "https://via.placeholder.com/450x50",
    "https://via.placeholder.com/220x30",
    "https://via.placeholder.com/150",
    "https://via.placeholder.com/50x50"
]

//pour chaque url du tableau
//créer un element img dont l'attribut src 
//contiendra un url 

let el_contenu = document.getElementById("contenu");

function afficherImages(tableau_urls){
    for (const str_url of tableau_urls){
        //on crée un élément de type img
        let el_img = document.createElement("img");
        el_img.src = str_url;
        el_contenu.appendChild(el_img);
        el_contenu.appendChild(document.createElement("br"))
    }
}

afficherImages(images);
