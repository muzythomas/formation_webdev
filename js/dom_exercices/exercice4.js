let langages = [
    {
        nom: "Python",
        paradigmes: "Orienté objet, impératif et interprété"
    },
    {
        nom: "C",
        paradigmes: "Impératif, procédural, structuré"
    },
    {
        nom: "C++",
        paradigmes: "Générique, orienté objet, procédural"
    },
    {
        nom: "C#",
        paradigmes: "Structuré, impératif, orienté objet"
    },
    {
        nom: "Java",
        paradigmes: "Orienté objet, structuré, impératif, fonctionnel"
    },
    {
        nom:"Javascript",
        paradigmes: "Script, orienté prototype, impératif, fonctionnel"
    }
]

//Pour chaque objet du tableau
//afficher le nom dans un titre
//et les paradigmes dans un paragraphe

let contenu = document.getElementById("contenu");
/*
//version avec un for classique
for (let i = 0; i < langages.length; i++){
    //crée un element h1
    let el_titre = document.createElement("h1");
    //change le contenu textuel de cet element pour y placer
    //le contenu de la propriété nom de notre objet
    el_titre.textContent = langages[i].nom;
    //ajoute notre element a notre page
    contenu.appendChild(el_titre);
    //crée un element p
    let el_paragraphe = document.createElement("p");
    //change le contenu textuel de cet element p pour y placer
    //le contenu de la propriété paradigmes de notre objet
    el_paragraphe.textContent = langages[i].paradigmes;
    //ajoute notre element a notre page
    contenu.appendChild(el_paragraphe);
}
*/
for (const langage of langages){
    let el_titre = document.createElement("h1");
    el_titre.textContent = langage.nom;

    contenu.appendChild(el_titre);

    let el_paragraphe = document.createElement("p");
    el_paragraphe.textContent = langage.paradigmes;

    contenu.appendChild(el_paragraphe);
}
