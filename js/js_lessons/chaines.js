// ' = quote
// " = guillemet
// / = slash
// \ = back slash ou anti slash
// ` = back quote

//CHAÎNES DE CARACTERES

//une chaine de caractères
let chaine = "ceci est une chaine";

//concaténation
let chaine2 = "ceci est " + "une chaine";

//inclusion de variables
let mot = "cool";
let chaine3 = `les chaînes c'est ${mot}`; // les chaînes c'est cool

//echappage
//échapper un caractère permet de le traiter comme 
//faisant partie d'une chaine et non de la syntaxe javascript
let chaine4 = "ça s'appelle une \"chaine\"";

//retour à la ligne
//opérateur new line \n
let chaine5 = "retour à la ligne\n";

//longueur d'une chaîne
let longueur = chaine5.length;
let longueur2 = "une chaine".length;


//changer la casse
//toUpperCase() et toLowerCase() renvoient des copies des chaines
//avec la casse modifiée, et ne modifient pas la variable initiale
const mot_a_capitaliser = "bonjour";
console.log(mot_a_capitaliser.toUpperCase()); //BONJOUR 
const mot_a_minimiser = "SALUT";
console.log(mot_a_minimiser.toLowerCase()); //salut


//comparaison de chaines 
const chaine6 = "toto";
console.log(chaine6 === "toto"); //true
console.log(chaine6 === "tata"); //false

console.log(chaine6 === "Toto"); //false

//éliminer les différences de casse
console.log(chaine6.toLowerCase() === "Toto".toLowerCase()); //true

//Accéder a un caractère en utilisant son indice
//                 0    1    2    3
//               ['N', 'i', 'c', 'o'...] 
const chaine7 = "Nico vaut pas 3 kopecs";
console.log(chaine7[0]);// le premier caractère, N
console.log(chaine7[8]); // 8eme caractère, t


//parcourir une chaine de caractères
const chaine8 = "coucou je suis une chaine plutôt longue";
for (let i = 0; i < chaine8.length; i++){
    console.log(chaine8[i]);
}

const chaine9 = "Mot";
//parcours la chaine et affiche chaque lettre
for (const lettre of chaine9){
    console.log(lettre);
}
// affiche la première lettre M et la dernière lettre t
console.log(chaine9[0], chaine9[chaine9.length - 1]); 

//convertir une chaine en tableau
const tab_chaine8 = Array.from(chaine8);
console.log(tab_chaine8);

//recuperer l'index d'un élément
const phrase = "vive le vent, vive le vendredi";
console.log(phrase.indexOf("e"));

//vérifier le début ou la fin d'une chaine
console.log(phrase.startsWith("vive")); //true
console.log(phrase.startsWith("Vive")); //false

console.log(phrase.endsWith("dredi")); //true
console.log(phrase.endsWith("vent")); //false

//décomposer une chaine de caractère en sous parties
//split sépare une chaine de caractères en sous chaines
//a partir d'un séparateur
//exemple renvoie toutes les sous chaines séparées par un espace
//split renvoie le résultat dans un tableau de chaines
const tab_phrase = phrase.split(" ");
console.log(tab_phrase); //["vive","le","vent,","vive","le","vendredi"]
console.log(tab_phrase[2]); //renvoie vent,

const jours = "lundi,mardi,mercredi,jeudi,vendredi,samedi,dimanche";
const tab_jours = jours.split(",");
                        //    0       1         2        3        4        5   
console.log(tab_jours); //["lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche"]
console.log(tab_jours[0][0]); // première lettre du premier élément


//EXERCICE
//Compter le nombre de voyelles dans une chaine de caractères

function compteVoyelles(chaine){
    let compteur = 0;
    let minuscule = chaine.toLowerCase();
    for (let i = 0; i < minuscule.length; i++){
        if (minuscule[i] === "a" || 
        minuscule[i] === "e" ||
        minuscule[i] === "i" ||
        minuscule[i] === "o" ||
        minuscule[i] === "u" ||
        minuscule[i] === "y") {
            compteur++;
        }
    }
    return compteur;
}

console.log(compteVoyelles("COmbien de voyElles ?"));//renvoie 8

function compteVoyellesBis(chaine){
    const voyelles = ["a","e","i","o","u","y","é","è","ë", "ê" ,"ï", "î", "ö", "ô"];
    let compteur = 0;
    let minuscule = chaine.toLowerCase();
    for (let i = 0; i<minuscule.length; i++){
        if (voyelles.includes(minuscule[i])){
            compteur++;
        }
    }
    return compteur;
}
console.log(compteVoyellesBis("COmbién de voyèlles ?"));//renvoie 8

// tableau.includes(element) 
// renvoie true si l'element est contenu dans le tableau

//EXERCICE
//Ecrire une fonction qui prend une chaine en entrée
//et renvoie la chaine écrite à l'envers

function inverserChaine(chaine){
    let resultat = "";
    for (let i = chaine.length - 1; i >= 0; i--){
        resultat = resultat + chaine[i];
    }
    return resultat;
}

function inverserChaineBis(chaine){
    let resultat = "";
    for (let i = 0; i < chaine.length; i++){
        resultat = chaine[i] + resultat;
    }
    return resultat;
}

console.log(inverserChaine("!tulaS"));
console.log(inverserChaineBis("!tulaS"));
//EXERCICE
//Ecrire une fonction qui renvoie true 
//si un mot est un palindrome
//false sinon

function estPalindromeMot(mot){
    let minuscule = mot.toLowerCase();
    return minuscule === inverserChaine(minuscule);
}

console.log(estPalindromeMot("Radar")) //renvoie true
console.log(estPalindromeMot("coucou")) //renvoie false

//VERSION DIFF 
//Ecrire une fonction qui renvoie true
//si une PHRASE est un palindrome
//false sinon

function estPalindrome(phrase){
    let mot = "";
    for (let i = 0; i < phrase.length; i++){
        if (" " !== phrase[i]){
            mot += phrase[i];
        }
    }
    return estPalindromeMot(mot);
}
console.log(estPalindrome("la mariee ira mal")); //renvoie true
console.log(estPalindrome("cochon qui rit")); //renvoie false
console.log(estPalindrome("   la     mariee ira mAL")); //renvoie true

//EXERCICE 
//Ecrire une fonction qui élimine les caractères accentués 
//d'une chaine et les remplaces par leur version 
//non accentuée

function retireAccents(chaine){

}

console.log(retireAccents("maïs braisé")); //renvoie "mais braise"
console.log(retireAccents("éèàçïô")); //renvoie "eeacio"