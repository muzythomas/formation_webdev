//Dans les langages orientés objet on peut utiliser 
//une structure de données appelée Objet
//qui permet l'organisation de données de façon efficace
//et systématique aidant ainsi à représenter l'existant
//dans nos programmes
//Un stylo possèderait donc des propriétés le représentant comme
const stylo = {
    type: "bille", //un type de pointe
    couleur: "bleu", //la couleur de l'encre
    marque: "Bic", //sa marque
    encre_restante: 100 //ou une variable destinée a changer comme le niveau d'encre
};

//malgré le fait que notre stylo soit une constante
//ses propriétés ne le sont pas forcément
//et on peut y accéder en précisant le nom de notre objet
//puis le nom de sa propriété, le tout séparé d'un .
console.log(stylo.couleur);
console.log(`Il reste ${stylo.encre_restante}% d'encre`);
stylo.encre_restante = 98;
console.log(`Il reste ${stylo.encre_restante}% d'encre`);


const jones_car ={
    nom: "J-2000",
    taille_reservoir : 40,
    vitesse_max : 220,
    chevaux : 225,
    vitesse : 0
};

const jones = {
    nom: "Jones Jones",
    points_de_vie: 100,
    points_de_mana: 50,
    force: 20,
    xp: 0,
    
    //un objet peut posséder des méthodes qui lui sont propres
    //permettant d'intéragir avec ses propriétés
    description(){
        //cette fonction fournit une description de notre personnage 
        //et liste ses caractéristiques 
        return `${this.nom} a ${this.points_de_vie} points de vie restants,
         ${this.points_de_mana} de mana,
        et ${this.force} de force, 
        et ${this.xp} points d'expérience`;
        
    },

    afficher_xp(){
        return `Jones a ${this.xp} points d'expérience`;
    }
};

//Tout comme une propriété, une méthode s'appelle à l'aide de son nom
console.log(jones.description());

console.log("Jones se prend un coup de poing");
//pour modifier les propriétés on les utilise
// comme des variables
jones.points_de_vie = jones.points_de_vie - 15;

console.log("Jones trouve des gants de force +10");
jones.force = jones.force + 10;


console.log(jones.description());

console.log("Jones a vaincu son ennemi");
jones.xp = jones.xp + 10;

console.log(jones.afficher_xp());


const chien = {
    nom : "Médor",
    race : "Dalmatien",
    robe : "tacheté",
    taille : 47.8,
    aboiement: "OUAF OUAF",

    aboyer(){
        return `${this.aboiement}`;
    }
}

console.log(chien.aboyer());