
/*
=== égalité stricte
!== différence stricte
>= supérieur ou égal
> strictement supérieur 
<= inférieur ou égal
< strictement inférieur
== égalité 
!= différence 
 */
const age = prompt("Votre âge");
if ((age < 18) && (age > 0)){
    console.log("vous êtes mineur");
    
} else if (age > 0){
    console.log("vous êtes majeur");
}
/*
if (age >= 18){
    console.log("Vous êtes majeur");
} else if (age < 0) 
    {
        console.log("Tutmokdemoi");
    } else {

        console.log("Vous êtes mineur");
}
*/

console.log(age >= 18);

if ((age < 0) || (age > 127)){
    console.log("âge invalide");
}

/*
Booléens
&&
true && true === true
true && false === false
false && true === false
false && false === false

||
true || true === true
true || false === true 
false || true === true
false || false === false

!
!true = false
!false = true
*/

if (!(age > 100)) {
    console.log("age inférieur ou égal à 100");
}

let fruit = prompt("votre fruit préféré ?");
switch (fruit){
    case "Orange": 
        console.log("Vous pouvez faire du jus");
        break;
    case "Banane":
        console.log("Vous pouvez faire un cake aux bananes");
        break;
    case "Pomme":
        console.log("Vous pouvez faire une tarte aux pommes");
        break;
    default: 
        console.log("Vous êtes capricieux")
}