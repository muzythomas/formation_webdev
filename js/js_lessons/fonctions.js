//une fonction possède un nom
//ce nom doit représenter explicitement son rôle
function direSalut() {
    //le corps de la fonction dicte ce qui se passera au moment de l'appel
    console.log("Salut");
}

//pour appeler une fonction on appelle simplement son nom
direSalut();

//une fonction peut prendre des arguments en entrée
//ces arguments sont utilisables en tant que variables dans le corps de la fonction
function multi(x, y){
    //le mot clé return permet de renvoyer une valeur et terminer l'exécution
    return x*y;
}

let a = 40;
let b = 403;
//en utilisant return on peut stocker le retour de la fonction dans une variable
//avec une simple affectation a une variable
let resultat = multi(a,b);
console.log(resultat);

//déclaration d'une fonction anonyme
//une fonction anonyme se stocke dans une variable
const exclamation = function(message){
    return message + " !!";
}

console.log(exclamation("salut"));

//déclaration d'une fonction fléchée
//la fonction flechée est une fonction anonyme a la syntaxe particulière
//permettant d'être plus concis à l'aide d'une syntaxe plus courte
const interrogation = (message) => {
    return `${message} ??`;
}

console.log(interrogation("salut2"));

//une fonction fléchée n'effectuant qu'un return 
//n'a pas besoin du mot clé, il est dans ce cas implicite
const interrogation2 = (message) => `${message} ??`;
console.log(interrogation2("salut3"));

//Javascript possède ses propres fonctions internes
console.log(Math.random());

//On peut les appeler directement ou les utiliser pour écrire 
//nos propres fonctions
//Dans ce cas, on génère un nombre aléatoire entre min (inclus)
//et max (exclu)
function nombreAleatoire(min, max){
    return Math.floor(Math.random() * (max-min) + min);
}

console.log(nombreAleatoire(5, 8));


//une fonction qui renvoie le nombre passé en paramètre
//au carré
function nombreAuCarre(num){
    return num*num;
}

function squared(num){
    return Math.pow(num, 2);
}

console.log(squared(45));