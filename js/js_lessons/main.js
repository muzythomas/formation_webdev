//Modifier une page HTML

//on récupère l'element avec l'id langages
let langages = document.getElementById("langages");

//on modifie le contenu de l'élément 
//langages.innerHTML = "coucou";

//on ajoute du contenu a l'interieur 
langages.innerHTML += "<li>Coucou</li>";

let titre = document.getElementById("titre");
titre.textContent += "!";

//changer la valeur d'un attribut 
titre.setAttribute("id", "titre1");
titre.id = "titre2";

//changer les classes d'un element
let li_javascript = document.getElementById("javascript");
//ajoute une classe
li_javascript.classList.add("interprete");
//retire une classe 
li_javascript.classList.remove("objet");

//ajouter des elements dans le DOM
//creation d'un element li
let el_ruby = document.createElement("li");
//assignation d'un id à cet element
el_ruby.id = "ruby";
//definition du contenu textuel de l'element
el_ruby.textContent = "Ruby";
//ajoute d'une classe a notre element
el_ruby.classList.add("objet");
//appendChild rajoute un enfant a un element 
//et le fait a la suite des autres
langages.appendChild(el_ruby);
//prepend ajoute un element et le fait avant les autres
langages.prepend(el_ruby);

//insère un element avant un autre dans un 
//element parent
//parent.insertBefore(insertion, emplacement);
langages.insertBefore(el_ruby,
    document.getElementById("cplusplus"));

let el_basic = document.createElement("li");
el_basic.id = "basic";
el_basic.textContent = "Basic";

//replace un element par un autre 
//dans un element parent
//parent.replaceChild(remplacant, element)
langages.replaceChild(el_basic, 
    document.getElementById("csharp"));

//supprimer un element 
langages.removeChild(document.getElementById("java"));

//creer un lien
//on cree une ancre
let lien = document.createElement("a");
//on definit son attribut href
lien.href = "https://www.google.fr";
//on definit son contenu textuel
lien.textContent = "google"
//on l'ajoute dans un element li
el_basic.appendChild(lien);

