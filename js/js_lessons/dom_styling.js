let el_p = document.querySelector("p");
el_p.style.color = "red";
el_p.style.padding = "15px";
el_p.style.backgroundColor = "black";

let elements_p = document.getElementsByTagName("p");
console.log(elements_p[0].style.color);
console.log(elements_p[1].style.color);
console.log(elements_p[2].style.color);

let el_p3 = document.getElementById("p3");
let style_el_p3 = getComputedStyle(el_p3);
console.log(style_el_p3.color);
/*
//prompt fait apparaitre une fenêtre permettant
//de demander une donnée a un utilisateur
let couleurTexte = prompt("Entrer couleur du texte");
let couleurFond = prompt("Entrer la couleur du fond");
//on recupere un tableau d'elements div
let elements_div = document.getElementsByTagName("div");
//pour chaque element div de ce tableau
for (let i = 0; i < elements_div.length; i++){
    //on change la couleur du texte et du fond
    //avec les couleurs récupérées via prompt
    elements_div[i].style.color = couleurTexte;
    elements_div[i].style.backgroundColor = couleurFond;
} 
for (const div of elements_div){
    div.style.color = couleurTexte;
    div.style.backgroundColor = couleurFond;
}
*/
//on récupère l'id du div a modifier via prompt
let id_div = prompt("Entrer l'id de l'element a modifier");
//on recupere l'element div via l'id récupéré
let div_a_modifier = document.getElementById(id_div);
//on demande les couleurs a l'utilisateur
let couleurTexteDiv = prompt("Entrer couleur du texte");
let couleurFondDiv = prompt("Entrer la couleur du fond");
//on applique les couleurs
div_a_modifier.style.color = couleurTexteDiv;
div_a_modifier.style.backgroundColor = couleurFondDiv;

let padding_div = prompt("entrez la valeur du padding");

div_a_modifier.style.padding = padding_div;