let fin = 10
console.log("On compte jusqu'à " + fin);


let i = 1;
//la boucle while permet de préciser une condition de fin quelconque
//tant qu'il s'agit d'un booléen
while (i <= fin){
    console.log(i);
    i++;
}

//la boucle for permet d'effectuer une itération
//on précise les bornes inférieures et supérieures de notre boucle
//et on exécute le traitement à l'intérieur
for (let compteur = 1; compteur <= fin; compteur++){
    if ((compteur%2) === 0){
        console.log("pair : " + compteur);
    } else {
        console.log("impair: " + compteur);
    }
}