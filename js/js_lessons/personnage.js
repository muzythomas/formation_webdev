/*
const jones = {
    nom: "Jones Jones",
    points_de_vie: 100,
    points_de_mana: 50,
    force: 20,
    xp: 0,
    
    //un objet peut posséder des méthodes qui lui sont propres
    //permettant d'intéragir avec ses propriétés
    description(){
        //cette fonction fournit une description de notre personnage 
        //et liste ses caractéristiques 
        return `${this.nom} a ${this.points_de_vie} points de vie restants,
         ${this.points_de_mana} de mana,
        et ${this.force} de force, 
        et ${this.xp} points d'expérience`;
        
    },

    afficher_xp(){
        return `${this.name} a ${this.xp} points d'expérience`;
    }
};

console.log(jones.description());


const jane = {
    nom: "Jane Jane",
    points_de_vie: 90,
    points_de_mana: 80,
    force: 20,
    xp: 10,
    
    //un objet peut posséder des méthodes qui lui sont propres
    //permettant d'intéragir avec ses propriétés
    description(){
        //cette fonction fournit une description de notre personnage 
        //et liste ses caractéristiques 
        return `${this.nom} a ${this.points_de_vie} points de vie restants,
         ${this.points_de_mana} de mana,
        et ${this.force} de force, 
        et ${this.xp} points d'expérience`;
        
    },

    afficher_xp(){
        return `${this.nom} a ${this.xp} points d'expérience`;
    }
};

console.log(jane.description());

*/

// Une classe est créée avec le mot-clé class
//suivi du nom de la classe qui commence par une majuscule
/* class MaClasse {
    constructor(parametre1, parametre2...){
        this.propriete1 = parametre1;
        this.propriet2 = parametre2;
        ...
    }

    methode(){
        //corps de la methode
    }
}

//instancier une classe
mon_objet = new MaClasse(parametre1, parametre2...);
*/
class Personnage {
    constructor(nom, points_de_vie, points_de_mana, force) {
        this.xp = 0;
        this.mort = false;
        this.points_de_vie_max = 300;
        this.points_de_vie_min = 0;
        this.nom = nom;   
        this.force = force;
        if (points_de_vie > this.points_de_vie_max){
            this.points_de_vie = this.points_de_vie_max;
        } else {
            this.points_de_vie = points_de_vie;
        }
        this.points_de_mana = points_de_mana;
    }

    //définition d'une méthode
    decrire() {
        //cette methode fournit une description de notre personnage 
        //et liste ses caractéristiques 
        if (this.mort){
            return `${this.nom} est décédé`;
        } else {
            return `${this.nom} a ${this.points_de_vie} points de vie restants,
            ${this.points_de_mana} de mana,
            et ${this.force} de force, 
            et ${this.xp} points d'expérience`;
        }
    }

    estMort(){
        this.mort = this.points_de_vie <= this.points_de_vie_min;
    }

    retirerPointsDeVie(n){
        this.points_de_vie -= n;
        if (this.points_de_vie < this.point_de_vie_min){
            this.points_de_vie = this.points_de_vie_min;
        }
        this.estMort();
    }

    ajouterPointsDeVie(n){
        this.points_de_vie += n;
        if (this.points_de_vie > this.points_de_vie_max){
            this.points_de_vie = this.points_de_vie_max;
        }
        this.estMort();
    }

    //préciser la valeur d'un paramètre dans la déclaration de fonction
    //permet de préciser une valeur par défaut d'un paramètre
    //cette valeur par défaut sera utilisée en cas d'absence d'argument à l'appel de la fonction
    ressuciter(n = (this.points_de_vie_max*0.1)){
        if (this.mort){
            this.ajouterPointsDeVie(n);
            console.log(`Par la grâce des grands anciens, ${this.nom} retrouve la vie !`);
        } else {
            console.log(`Pas besoin de ressuciter ${this.nom}, il est toujours en vie`);
        }
    }

    attaquer(cible) {
        //si le personnage attaquant est en vie
        if (!this.mort){
            //si la cible existe
            if (cible !== undefined) {
                //si la cible est en vie
                if (!cible.mort){
                    //on retire un nombre de points de vie à la cible
                    //égal à la force du personnage attaquant
                    console.log(`${this.nom} attaque ${cible.nom} !`);
                    cible.retirerPointsDeVie(this.force);
                    if (cible.mort){
                        console.log(`${cible.nom} a été tué des suites du coup porté par ${this.nom}`);
                    }
                } else {
                    console.log(`${this.nom}.attaquer : Erreur, ${cible.nom} est mort`);
                }
            } else {
                console.log(`${this.nom}.attaquer : Erreur, cible non définie`);
            }
        } else {
            console.log(`${this.nom}.attaquer : Erreur, ${this.nom} est mort`);
        }
    }

    attaquerPlusieursFois(cible, n){
        for (let i = 0; i < n; i++){
            this.attaquer(cible);
        }
    }  

}

const jones = new Personnage("Jones", 100, 50, 25);
const jane = new Personnage("Jane", 90, 80, 20);

console.log(jones.force, jane.force);
console.log(jones.decrire());

const mechant = new Personnage("Agrougrou Mechant", 200, 0, 10);

console.log(mechant.decrire());

jones.attaquer(mechant);
jane.attaquer(mechant);
jones.attaquer(); //invalide
console.log(mechant.decrire());

jones.attaquerPlusieursFois(mechant, 7);
console.log(mechant.decrire());

mechant.ressuciter();
console.log(mechant.decrire());

//HERITAGE
//le mot clé extends permet de créer une sous classe à partir d'une classe dite mère
//pour appeler le constructeur de l'objet précédent on utilise le mot clé super
class SuperPersonnage extends Personnage{
    constructor(nom, points_de_vie, points_de_mana, force, dexterite){
        super(nom, null, points_de_mana, force);
        this.points_de_vie = points_de_vie;
        this.points_de_vie_max = 500;
        if (points_de_vie > this.points_de_vie_max){
            this.points_de_vie = this.points_de_vie_max;
        } 
        this.dexterite = dexterite;
    }

    decrire(){
        return super.decrire() + 
        ` et ${this.dexterite} points de déxterité`;
    }
}

const super_jones = new SuperPersonnage("Super Jones", 900, 50, 20, 20);

console.log(super_jones.nom);
console.log(super_jones.dexterite);
console.log(super_jones.decrire());
super_jones.attaquer(jones);
console.log(super_jones.decrire());