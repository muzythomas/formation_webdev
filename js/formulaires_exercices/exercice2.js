let voitures = [
    {
        code: "TO",
        nom: "Toyota"
    },
    {
        code: "AR",
        nom: "Alfa Romeo"
    },
    {
        code: "CI",
        nom: "Citroën"
    },
    {
        code: "MA",
        nom: "Mazda"
    },
    {
        code: "RE",
        nom: "Renault"
    }
];

//renvoie un tableau contenant différents modèle d'une marque a partir d'un code
function getModel(codeMarque){
    switch(codeMarque){
        case "TO":
            return ["Yaris", "Corolla", "Auris", "Rav4"];
        case "AR":
            return ["Giulia", "Giulietta", "Mito", "159"];
        case "CI":
            return ["C3", "C5", "AX", "ZX"];
        case "MA":
            return ["Mazda2", "Mazda3", "Mazda Cx-5", "Mazda Mx5"];
        case "RE":
            return ["Talisman", "Scenic", "Kangoo", "Captur", "Twingo", "Megane"];
        default: 
            return [];
    }
}


//Ajouter le code nécessaire pour remplir la liste de marques 
//au chargement de la page

//Ajouter le code nécessaire pour que la liste de chaque modèle
//s'affiche dans la liste ul d'id voitures à chaque fois que l'utilisateur
//selectionne une marque de voiture dans la liste


let el_select_marque = document.getElementById("marque");

//ajout de chaque optoin dans notre select
for (const voiture of voitures){
    let option = document.createElement("option");
    option.value = voiture.code;
    option.textContent = voiture.nom;
    console.log(option);
    el_select_marque.appendChild(option);
}

el_select_marque.addEventListener("change", function(event){
    //on récupère notre liste de modèles à partir d'un code de marque
    //ce code de marque est contenu dans l'attribut value de notre select
    let tab_modeles = getModel(event.target.value);
    //on récupère le ul dans lequel nous allons ajouter nos modèles
    let el_ul = document.getElementById("voitures");
    //on le vide
    el_ul.innerHTML = "";
    //on ajoute les modèles contenus dans notre tableau
    for (const modele of tab_modeles){
        let el_li = document.createElement("li");
        el_li.textContent = modele;
        el_ul.appendChild(el_li);
    }
});