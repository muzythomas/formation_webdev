//Vérifier que les mots de passe et email
//soient identiques dans les champs de confirmation
//Vérifier que le mot de passe fasse au moins 8 caractères
//Afficher les messages d'aide dans les elements span
//info_mdp et info_email

let mdp1 = document.getElementById("mdp1");
let mdp2 = document.getElementById("mdp2");

let email1 = document.getElementById("email1");
let email2 = document.getElementById("email2");

let info_mdp = document.getElementById("info_mdp");
let info_email = document.getElementById("info_email");


mdp1.addEventListener("input", function(){
    motsDePasseIdentiques();
    if (mdp1.value.length < 10){
        info_mdp.textContent = " Mot de passe trop court."
    }
})

mdp2.addEventListener("input", function(){
    motsDePasseIdentiques();
});
function motsDePasseIdentiques(){
    if (mdp1.value === mdp2.value){
        info_mdp.textContent = "mdp identiques";
    } else {
        info_mdp.textContent = "mdp differents";
    }
}

email1.addEventListener("input", emailsIdentiques);
email2.addEventListener("input", emailsIdentiques);

function emailsIdentiques(){
    if (email1.value === email2.value){
        info_email.textContent = "email identiques";
    } else {
        info_email.textContent = "email differents";
    }
}