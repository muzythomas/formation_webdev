/**
 * Affiche des appartements dans un conteneur
 * @param {array} tab_appartements un tableau d'objets appartements
 * @param {HTMLElement} el_container un conteneur pour afficher les appartements
 */
function displayAppartements(tab_appartements, el_container) {
    //on vide le conteneur avec la liste d'appartements
    el_container.innerHTML = "";
    //pour chaque appartement
    for (const element of tab_appartements) {
        //je crée une div pour mon appartement
        let div_element = document.createElement("div");
        //on affecte l'id de l'appartement à la div
        div_element.id = element.id;

        let h3_price = document.createElement("h3");
        h3_price.textContent = `Prix/mois : ${element.price} €`;

        let p_nb_rooms = document.createElement("p");
        p_nb_rooms.textContent = `F${element.rooms} (${element.rooms} pièces)`

        let p_surface_area = document.createElement("p");
        p_surface_area.textContent = `${element.surface} m²`;

        let p_complete_address = document.createElement("p");
        p_complete_address.textContent = `${element.address}${element.zip !== undefined ? ', ' + element.zip : ''}, ${element.city}`;

        let img_appart_photo = document.createElement("img");
        img_appart_photo.src = element.photo;

        div_element.appendChild(img_appart_photo);
        div_element.appendChild(h3_price);
        div_element.appendChild(p_surface_area);
        div_element.appendChild(p_nb_rooms);
        div_element.appendChild(p_complete_address);
        el_container.appendChild(div_element);
    }
}

/**
 * filterAppartements prend en paramètre un tableau d'appartements
 * les valeurs de filtrage numériques val_price_low, val_price_up, val_rooms, val_area
 * et renvoie un tableau d'appartements filtrés
 */
function filterAppartements(tab_appartements, val_price_low, val_price_up, val_rooms, val_area){
    let arr_filtered = [];
    for (const appart of tab_appartements){
        if (appart.price >= val_price_low && appart.price <= val_price_up && appart.rooms >= val_rooms && appart.surface >= val_area){
            arr_filtered.push(appart);
        }
    }
    return arr_filtered;
}

//préparation de l'url
let json_url = "http://localhost/~ziroshell/formation/js/requetes_exercices/apparts.json"

//préparation des éléments du DOM
let div_appartement_list = document.getElementById("appartement_list");

let filter_price_low = document.getElementById("filter_price_low");
let filter_price_up = document.getElementById("filter_price_up");
let filter_rooms = document.getElementById("filter_rooms");
let filter_area = document.getElementById("filter_area");

let arr_response;

//récupération du fichier
ajaxGet(json_url, function (response) {
    arr_response = JSON.parse(response);

    document.getElementsByTagName("form")[0].addEventListener("input", function(event){
        //on récupère le formulaire ciblé
        let form = event.target.form;
        //on récupère les valeurs de chaque champ de notre formulaire
        let val_price_low = form.filter_price_low.value;
        let val_price_up = (form.filter_price_up.value ? form.filter_price_up.value : Infinity);
        let val_rooms = form.filter_rooms.value;
        let val_area = form.filter_area.value;
        //on appelle la fonction filtre
        let arr_filtered = filterAppartements(arr_response, val_price_low, val_price_up, val_rooms, val_area );
        displayAppartements(arr_filtered, div_appartement_list);
    })

    displayAppartements(arr_response, div_appartement_list)
})


