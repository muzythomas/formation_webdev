let requete = "";
let postcode = "";
document.getElementById("form_recherche").addEventListener("submit", function(event){
    requete = event.target.recherche.value;
    postcode = event.target.postcode.value;
    ajaxGet(`https://api-adresse.data.gouv.fr/search/?q=${requete}&postcode=${postcode}`, function(response){
        console.log(JSON.parse(response));
    })
    event.preventDefault();
})
