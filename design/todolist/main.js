let buttonAdd = document.getElementById('add');
let inputAdd = document.getElementById('input');
let todoList = document.getElementById('todolist');
let completedList = document.getElementById('completed');

let listArray = [];

buttonAdd.addEventListener('click', function(event){
    let text = inputAdd.value;
    let listItem = createListItem(text);
    addToList(listItem);
});

function addToList(listItem){
    todoList.appendChild(listItem);
}

function createListItem(text){
    let listItem = document.createElement('li');

    let buttonDelete = document.createElement('button');
    buttonDelete.innerText = "Delete";
    buttonDelete.addEventListener('click', function(event){
        removeFromList(event.target.parentElement);    
    });

    let buttonComplete = document.createElement('button');
    buttonComplete.innerText = "Complete";
    buttonComplete.addEventListener('click', function(event){
        moveToComplete(event.target.parentElement);
        event.target.disabled = "true";
    });

    listItem.innerText = text;
    listItem.appendChild(buttonComplete);
    listItem.appendChild(buttonDelete);

    return listItem;
}

function removeFromList(listItem){
    let grandParent = listItem.parentElement;
    listItem.classList.add('slide-out');
    setTimeout(function(){
        grandParent.removeChild(listItem);
    }, 150);
}

function moveToComplete(listItem){
    listItem.classList.add('slide-out');
    setTimeout(function(){
        removeFromList(listItem);
        listItem.classList.remove('slide-out');
        completedList.prepend(listItem);
        listItem.style.textDecoration = "line-through";
    }, 299);
    
}
