//récupère les positions x et y de la souris, à utiliser dans un eventListener
function getMousePos(event) {
    let x = 0;
    let y = 0;

    if (!event) {
        let event = window.event;    
    }

    if (event.pageX || event.pageY){
        x = event.pageX;
        y = event.pageY;
    } else if (event.clientX || event.clientY) {
        x = event.clientX + document.body.scrollLeft 
            + document.documentElement.scrollLeft;
        y = event.clientY + document.body.scrollTop 
        + document.documentElement.scrollTop;
    }

    return {x: x, y: y};

}

let nav;
let nav_content;
document.addEventListener("DOMContentLoaded", function(event) {
    nav = document.getElementsByTagName("nav")[0];
    nav_content = document.getElementsByClassName("nav-content")[0];
    nav.addEventListener("mouseover", function(event){
       nav.classList.add("unfold");
        nav_content.classList.add("content-unfold")
    });

    nav.addEventListener("mouseout", function(event){
        nav.classList.remove("unfold");

        nav_content.classList.remove("content-unfold")
        
    });
    
    document.addEventListener("mousemove", function(event){
        let mousePos = getMousePos(event);
        //permet de faire quelque chose avec la position de la souris
    });
});


