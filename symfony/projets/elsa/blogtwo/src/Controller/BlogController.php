<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Article;
use App\Entity\Comment;
use App\Form\ArticleType;
use App\Form\CommentType;
use App\Form\RegistrationType;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog", name="blog")
     */
    public function index(ArticleRepository $repo)
    {
       
        $articles= $repo->findAll();
        return $this->render('blog/index.html.twig', [
            'controller_name' => 'BlogController',
            'articles'=>$articles
        ]);
    }


    /**
     * @Route("/" , name="accueil")
     */
    public function home()
    {
        return $this->render('blog/accueil.html.twig',[
            'title'=>'bienvenue dans le monde fabuleux des champignons'
        ]);

    }

     /**
     * @Route("/blog/new", name="blog_create")
     * @Route("/blog/{id}/edit", name="blog_edit")
     */

     //pas besoin de passer l'id dans param de fonction grace a param converter qui va comprendre qu'ul faut recup l'article avec cet id
    public function form(Article $article = null, Request $request, ObjectManager $manager)
    {
       
        //si article null par defaut cree en un, parcontre si je lui passe un id et edit va preremplir l'article pour pouvoir le modifier
        if(!$article){

        $article= new Article();
        }
        $form=$this->createForm(ArticleType::class,$article);

        //$form= $this->createFormBuilder($article)
          //          ->add('title')
          //          ->add('image')
            //        ->getForm();

        $form->handleRequest($request);

         if($form->isSubmitted()&& $form->isValid()){
             //si l'article n'a pas d'id alors cree une date
             if(!$article->getId()){
                $article->setCreateAt( new \DateTime());
             }
           
            $manager->persist($article);
            $manager->flush();
            //apres avoir executer la requete,revoie à la fonction show en lui precisant l'article crée ici via son id
            return $this->redirectToRoute('blog_show',[
                'id'=>$article->getId()
            ]);
         }
        return $this-> render('blog/create.html.twig',[
            'formArticle'=>$form->createView(),
            'editMode' =>$article->getId() !== null
            //si id de l'article different de null renvoie un form different avec editmode est une variable qui permet de savoir si on est en mode edition ou pas
        ]);  
    }




    /**
     * @Route("/blog/{id}", name="blog_show")
     */
    public function show(Article $article , Request $request, ObjectManager $manager)
    {
        //doctrine va chercher avec repository dans la classe Article l'id de l'article
       $comment= new Comment;
       $form= $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if($form->isSubmitted()&& $form->isValid()){
            $comment->setCreateAt( new \DateTime())
                    ->setArticle($article);
            $manager->persist($comment);
            $manager->flush();
            
            return $this->redirectToRoute('blog_show', ['id'=> $article->getId()
            ]);
        }

       
        return $this->render('blog/show.html.twig', [
            'article'=>$article,
            'commentForm'=>$form->createView()
        ]);
    }

   
    /**
     * @Route("/inscription", name="security_registration")
     */
    public function registration(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder){
        $user= new User();

        $form=$this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);
        if($form->isSubmitted()&& $form->isValid()){
            $hash = $encoder->encodePassword( $user, $user->getPassword());
            $user->setPassword($hash);
           
            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('app_login');
        }
        return $this->render('blog/registration.html.twig',[
            'form'=>$form->createView()
        ]);
    
    }
}