<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $user = new User();
        $user->setUsername('Test1');
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'mot_de_passe'
        ));
        $manager->persist($user);

        $user2 = new User();
        $user2->setUsername('Test2');
        $user2->setPassword($this->passwordEncoder->encodePassword(
            $user2,
            'mot_de_passe2'
        ));
        $manager->persist($user2);

        $user3 = new User();
        $user3->setUsername('Test3');
        $user3->setPassword($this->passwordEncoder->encodePassword(
            $user3,
            'mot_de_passe3'
        ));
        $manager->persist($user3);

        $admin = new User();
        $admin->setUsername('Admin');
        $admin->setPassword($this->passwordEncoder->encodePassword(
            $admin,
            'admin_mdp'
        ));
        $admin->setRoles(['ROLE_ADMIN']);
        $manager->persist($admin);

        $manager->flush();
    }
}
