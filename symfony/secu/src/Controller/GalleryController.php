<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Picture;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class GalleryController extends AbstractController
{
    /**
     * @Route("/", name="gallery_index")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(Picture::class);

        $listPictures = $repository->findAll();

        return $this->render('gallery/index.html.twig', [
            'pictures' => $listPictures,
        ]);
    }

    /**
     * @Route("/add", name="gallery_add")
     */
    public function add(Request $request)
    {
        $user = $this->getUser();

        $pic = new Picture();

        $form = $this->createFormBuilder($pic)
        ->add('name', TextType::class)
        ->add('description', TextType::class)
        ->add('path', FileType::class, ['label' => "Image (JPG, PNG)"])
        ->add('submit', SubmitType::class, ['label' => 'Poster Photo'])
        ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $file = $form->get('path')->getData();

            $uniqueName = md5(uniqid());
            $filename = $uniqueName.'.'.$file->guessExtension();

            try {
                $file->move(
                    $this->getParameter('img_dir'),
                    $filename
                );
            } catch (FileException $exception) {
                $this->redirectToRoute('gallery_index');
            }

            $pic = $form->getData();
            $pic->setPath($filename);
            $pic->setUser($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($pic);
            $entityManager->flush();
        }

        return $this->render('gallery/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
