<?php
//récupération de valeurs textuelles via la variable SUPERGLOBALE $_POST
echo $_POST['pseudo'].'</br>';
echo $_POST['message'].'</br>';
echo $_POST['liste'].'</br>';

//vérification d'initilisation d'une checkbox
if (isset($_POST['check'])){
    echo 'Case cochée</br>';
}

//récupération de valeur radio
echo $_POST['choix'].'</br>';
echo $_POST['date'].'</br>';

//print_r affiche le contenu d'une variable
//ici on affiche le contenu de la variable superglobale $_FILES
print_r($_FILES);

//On vérifie l'existence du fichier dans notre requête
//Et on vérifie que le transfert se soit fait sans erreur
if (isset($_FILES['photo']) && $_FILES['photo']['error'] == 0){
    //on vérifie que le fichier ne soit pas trop volumineux
    if ($_FILES['photo']['size'] < 100000){
        
        $info_file = pathinfo($_FILES['photo']['name']);
        $extension = $info_file['extension'];
        $tab_extension = array('jpg', 'jpeg', 'png', 'gif', 'svg');
        if (in_array($extension, $tab_extension)){
            //on déplace le fichier vers un emplacement permanent
            move_uploaded_file($_FILES['photo']['tmp_name'], 
            'uploads/'.$_FILES['photo']['name']);
            echo "envoi terminé";
        } else {
        echo "Type de fichier incorrect. Choisir parmis jpg,jpeg,png";
    }
        
    } else {
        echo 'Fichier trop volumineux';
    }
    
    
}


// $_FILES:
// ["photo" => ["name" => "burger", "extension" => "png", "size" = 1800]
//, photo2 => ["name" => "chat", "extension" => "jpg", "size" = 4800]]
?>