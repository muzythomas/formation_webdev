<?php 
//on vérifie que nos paramètres soient bien initialisés dans la requête
if (isset($_GET['nombre']) && isset($_GET['nom'])){
    //si c'est le cas on les stocke dans des variables
    $nombre = $_GET['nombre']; 
    $nom = $_GET['nom'];
    //on vérifie ensuite l'intégrité de nos données
    if (is_numeric($nombre) && !is_numeric($nom)){
        echo 'Resultat : ' . $nombre;
        echo 'Nom : ' . $nom;
    } else {
        //sinon on affiche un message d'erreur 
        echo 'Erreur, nombre doit être un nombre entier et 
        nom doit être une chaine de caractères';
    }

} else {
    //sinon on affiche un message d'erreur
    echo 'Erreur, paramètre manquant';
}
?>