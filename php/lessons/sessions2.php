<?php
//pour récuperer la session si elle existe on appelle session_start();
session_start();

print_r($_SESSION);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Titre 2</title>
</head>
<body>
    <h1> PAGE 2 </h1>
    <p>
        <!-- on peut accéder a nos variables de sessions de nouveau -->
       Coucou <?php echo $_SESSION['utilisateur'] ?>.    
    </p> 
    <p>
        Session ouverte à <?php echo $_SESSION['heure_debut']?>.
    </p>
    <a href="sessions.php">Page 1</a>
    <a href="deconnexion.php">Deconnexion</a>

</body>
</html>