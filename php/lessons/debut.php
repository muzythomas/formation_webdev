<?php
    $message = "Bonjour à tous"; //type string
    $nombre = 15; //type int
    $nombre_virgule = 15.5; //type float
    $booleen = true; //type bool
    $rien = NULL; //néant

    $age = 14;
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?php echo $message;  //on affiche notre message ici?></title> 

</head>
<body>
    <p>
        <h2>Une variable intégrée à une String</h2>
        <?php echo "Message de bienvenue : $message"; //intégrer une variable dans une string ?>
        
    </p>
    <p> 
        <h2>Une variable intégrée à une String par concaténation</h2>
        <?php echo 'Le nombre est :' . $nombre . ' !'; //concaténer des valeurs?>
    </p>

    <p>
        <h2>Utilisation des conditions</h2>
        <?php
    
        if ($age < 18){ //si age est inférieur a 18
            echo $age." ans : Vous êtes mineur"; //on affiche un message approprié
        } elseif ($age > 21){
            echo $age. " ans : Vous êtes majeur et en âge de boire";
        } else { //sinon 
            echo $age." ans : Vous êtes majeur"; //on affiche un message approprié
        } 
        ?>
    </p>
    <p>
    <h2>Utilisation des conditions</h2>

        <?php
            $afficher_message = false;
            if ($afficher_message){
                echo 'Message secret';
            } else {
                echo 'Vous n\'êtes pas autorisé à voir ce message';
            }
        ?>
    </p>
    <p>
    <h2>Utilisation des conditions avec AND/&&</h2>

        <?php
            $langue = "EN";
            $autorisation = true;
            $message_fr = "Bienvenue";
            $message_en = "Welcome"; 
            if ($autorisation && $langue == "FR"){ //&& et AND sont équivalents
                echo $message_fr;
            } elseif ($autorisation AND $langue == "EN"){ //&& et AND sont équivalents
                echo $message_en;
            }
        ?>
    </p>

    <p>
    <h2>Utilisation des conditions avec OR/||</h2>

        <?php
            $pays = "CH";
            if ($pays == "FR" OR $pays == "BE" || $pays == "CH"){ //OR et || sont équivalents
                echo $message_fr;
            } else {
                echo $message_en;
            }
        ?>
    </p>

    <p>
    <h2>Utilisation des conditions avec switch/case</h2>

        <?php
            $country = "US";
            switch ($country){
                case "FR":
                    echo "Vous êtes français";
                    break;
                case "UK":
                    echo "Vous êtes britannique";
                    break;
                case "BE":
                    echo "Vous êtes belge";
                    break;
                case "US":
                    echo "Vous êtes américain";
                    break;
                default:
                    echo "Pays non pris en compte";
            }
        ?>
        </p>
</body>
</html>