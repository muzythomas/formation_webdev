<?php
//differents mode de lecture
//r pour lecture seule, r+ pour lecture écriture
//mais uniquement si le fichier existe déjà
//a pour ecriture seule, a+ pour lecture écriture
//mais si le fichier n'existe pas, il sera créé
//on ouvre le fichier fichier.txt en lecture/ecriture
    $fichier = fopen('fichier.txt', 'r+');

    //tant que le fichier n'est pas arrivé a la fin
    while (!feof($fichier)){
        //on affiche une ligne et saute la ligne
        echo fgets($fichier) . '</br>';
    }
    //une fois à la fin on ajoute une ligne, puis un caractère de fin de ligne
    fputs($fichier, 'Coucou' . PHP_EOL);
    //enfin on ferme le fichier
    fclose($fichier);
?>