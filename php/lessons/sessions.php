<?php
//démarrage d'une session à l'aide de la fonction php
//session_start()
session_start(); //initialise la session et la superglobale $_SESSION

//pour affecter une variable valable pour toute la session
//on crée un couple clé => valeur qu'on ajoute à notre tableau
//$_SESSION
//isset verifie que la variable n'ait pas déjà été initialisée
if (!isset($_SESSION['heure_debut'])){
    $_SESSION['heure_debut'] = date('H:i:s');
}
$_SESSION['utilisateur'] = 'Roger';
if (isset($_SESSION['panier'])){
    foreach($_SESSION['panier'] as $produit){
        echo $produit . '</br>';
    }
}
print_r($_SESSION);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Titre</title>
</head>
<body>
<h1> PAGE 1 </h1>
    <p>
        <!-- On utilise les variables comme n'importe quelle autre -->
        Salut <?php echo $_SESSION['utilisateur'] ?>.    
    </p> 
    <p>
        Session ouverte à <?php echo $_SESSION['heure_debut']?>.
    </p>
    <a href="sessions2.php">Page 2</a>
    <a href="deconnexion.php">Deconnexion</a>
</body>
</html>