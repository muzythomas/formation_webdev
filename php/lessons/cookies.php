<?php
    //setcookie permet d'initialiser un cookie 
    //la première valeur est la clé du cookie
    //la seconde est la valeur du cookie
    //la troisième est la date d'expiration du cookie en secondes
    //time() permet d'obtenir la date actuelle en secondes (à partir de 1970 ou Unix timestamp)
    //et on ajoute 365 jours avant expiration avec un petit calcul
    setcookie('pseudo', 'jojo', time() + 365 * 3600 *24);
?>

<H1> Coucou </H1>

<!-- on accède aux cookies par le tableau contenu dans $_COOKIE -->
<?php
echo $_COOKIE['pseudo'];
?>