<?php
function bddConnect(String $host, String $db, String $user, String $password){
    try {
        $pdo = new PDO('mysql:host='.$host.';dbname='.$db.';charset=utf8', $user, $password);
        
    } catch (PDOException $exception) {
        die($exception);
    }
    return $pdo;
}

function bddSelect(PDO $connection, String $table, String $column){
    $query = $connection->query(sprintf('SELECT %s FROM %s;', $column, $table));
    return $query;
}

function bddSelectOrder(PDO $connection, String $table, String $column, String $by){
    $query = $connection->query(sprintf('SELECT %s FROM %s ORDER BY %s;', $column, $table, $by));
    return $query;
}

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Recherche apparts</title>
</head>
<body>
<form method="post">
    <select name="column">
        <option value="*">Tout</option>
        <option value="id">Identifiant</option>
        <option value="city">Ville</option>
        <option value="address">Adresse</option>
        <option value="zip">Code postal</option>
        <option value="surface">Surface</option>
        <option value="rooms">Nombre de Pièces</option>
        <option value="rent">Loyer</option>
    </select>
    <input type="submit" value="Envoyer">
</form>


<?php
if (isset($_POST['column'])){
    $colonne = $_POST['column'];

    $connection = bddConnect('localhost', 'test', 'root', 'password');
    $query = bddSelectOrder($connection, 'appart', '*', $colonne);

    echo '<table>';
    echo '<th>id</th>';
    echo '<th>city</th>';
    echo '<th>address</th>';
    echo '<th>zip</th>';
    echo '<th>surface</th>';
    echo '<th>rooms</th>';
    echo '<th>rent</th>';
    while ($data = $query->fetch()){ 
        echo '<tr>';
        echo '<td>'.$data['id'].'</td>';
        echo '<td>'.$data['city'].'</td>';
        echo '<td>'.$data['address'].'</td>';
        echo '<td>'.$data['zip'].'</td>';
        echo '<td>'.$data['surface'].'</td>';
        echo '<td>'.$data['rooms'].'</td>';
        echo '<td>'.$data['rent'].'</td>';
        echo '</tr>';
    }
    echo '</table>';
    $query->closeCursor();
}
?>
</body>
</html>