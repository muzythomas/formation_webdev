<?php

//le bloc try permet de tenter le code contenu dans son corps
//et si une erreur survient, permet de passer au corps du bloc catch
try{
    //pour se connecter à notre base de données, on utilise un objet PDO
    //PDO = PHP Data Object
    //PDO contient des méthodes permettant de dialoguer avec une base de données
    //ici, on contacte la base de données mysql, à l'adresse localhost
    //on demande à accéder la db nommée test, encodée en utf8
    //et on précise le nom d'utilisateur root, et le mot de passe password
    $bdd = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', 'password');
} catch (Exception $exception) {
    //en cas d'exception, on fait planter la page et affiche un message d'erreur
    die($exception->getMessage());
}

//on prépare notre requête sous forme de chaine de caractères
//qui contient la requête écrite en SQL 
//ici SELECT demande d'afficher des données, pas de les modifier
//* est un joker ou wildcard et indique que l'on désire récuperer toutes les propriétés
//FROM permet de préciser quelle table nous intéresse
$requete = 'SELECT * FROM article';
//ensuite on appelle notre PDO pour préparer la requête 
//on lui indique quelle sera la requête à exécuter lors de l'appel de fetch()
//on lui donne donc notre requête sql
//on stocke le resultat dans une variable, cette variable contiendra un objet
//cet objet pourra appeler fetch() et envoyer la requête au serveur de bdd
$reponse = $bdd->query($requete);

//ici on demande à notre objet renvoyé par query() de fetch() (c'est à dire aller chercher)
//le resultat. Ce résultat contiendra une ligne de notre table, que l'on stocke
//dans la variable $data
//la méthode fetch() renvoie également true si il existe une ligne à renvoyer
//et renvoie false si il n'y a  plus de lignes à récupérer
//ainsi notre while s'interromprait si il n'y avait plus de données à récupérer
while ($data = $reponse->fetch(PDO::FETCH_ASSOC)){ //$data = $reponse->fetch() permet de stocker les données dans $data 
                                    //tant qu'il y a des données à stocker
    //après avoir récupéré nos données, on peut itérer sur le tableau
    foreach ($data as $key => $value) {
        //et afficher son contenu, donc le contenu de notre ligne de base de données
        echo $key . '=>' . $value . '</br>';
    }
    //ici on saute juste une ligne pour y voir clair
    echo '_______</br>';
}
//on arrête le traitement de la requête
$reponse->closeCursor();
?>