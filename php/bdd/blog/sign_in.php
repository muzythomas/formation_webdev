<?php
//on démarre notre session
session_start();
//si un utilisateur est bien connecté, on le redirige automatiquement a l'accueil
if (!empty($_SESSION)){
    header('Location: index.php');
}
$page_title = "Connexion";
include('includes/header.php');
include('includes/menu.php');
?>
    <form action="sign_in_process.php" method="post">
        <ul class="form_list">
            <fieldset>
                <legend>Connexion</legend>
            <li><label for="username">Pseudo ou adresse mail</label></li>
            <li><input placeholder="j.jones" id="username" name="username" type="text" required></li>

            <li><label for="password">Mot de passe</label></li>
            <li><input placeholder="*******" name="password" id="password" type="password"></li>

            <li><button type="submit">Connexion</button><a href="sign_up.php">Je ne suis pas inscrit</a></li>
            
            </fieldset>
        </ul>
    </form>
    <div id="display_error">
        <?php 
            //si une erreur est renvoyée on l'affiche
            if (!empty($_GET['error_message'])){
                echo htmlspecialchars($_GET['error_message']);
            }
        ?>
    </div>
</body>
</html>
