<?php
//on commence notre session
session_start();
//si aucun utilisateur est connecté on redirige avec un message approprié
if (empty($_SESSION)) {
    $error_message = "Vous devez vous connecter pour poster un article.";
    header('Location: sign_in.php?error_message=' . $error_message);
}
$page_title = 'Edition d\'un article';
require 'includes/header.php';
require 'includes/menu.php';
?>
    <form action="article_post.php" method="post">
        <ul class="form_list">
            <fieldset>
                <legend>Edition d'un article</legend>
            <li><label for="title">Titre</label></li>
            <li><input placeholder="Votre titre" id="title" name="title" type="text" required></li>

            <li><label for="content">Corps</label></li>
            <li><textarea placeholder="Laissez aller votre imagination..." id="content" name="content" required></textarea></li>
            <li><button type="submit">Poster</button> <a href="index.php">Retourner aux articles</a></li>
            </fieldset>
        </ul>
    </form>
    <div id="display_error">
        <?php
if (!empty($_GET['error_message'])) {
    echo htmlspecialchars($_GET['error_message']);
}
?>
    </div>
<?php
include 'includes/footer.php';
?>
