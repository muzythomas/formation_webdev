<?php
//on commence toujours par démarrer notre session
session_start();
$page_title = "Inscription";
include('includes/header.php');
include('includes/menu.php');
?>
    <form action="sign_up_process.php" method="post">
        <ul class="form_list">
            <fieldset>
                <legend>Inscription</legend>
            <li><label for="username">Pseudo</label></li>
            <li><input placeholder="j.jones" id="username" name="username" type="text" required></li>

            <li><label for="email">e-mail</label></li>
            <li><input placeholder="j.jones@nasa.gov" id="email" name="email" type="email" required></li>

            <li><label for="password">Mot de passe</label></li>
            <li><input placeholder="*******" name="password" id="password" type="password" minlength=6 required></li>

            <li><label for="password_repeat">Confirmation mot de passe</label></li>
            <li><input placeholder="*******" name="password_repeat" id="password_repeat" type="password" minlength=6 required></li>
            
            <li><button type="submit">Inscription</button> <a href="sign_in.php">Je suis déjà inscrit</a></li>
            </fieldset>
        </ul>
    </form>
    <div id="display_error">
        <?php 
        //si une erreur est renvoyée on l'affiche
            if(!empty($_GET['error_message'])){
                echo htmlspecialchars($_GET['error_message']);
            }
        ?>
</div>
</body>
</html>
