<?php
//index.php affiche tous les articles de notre blog et les informations attachées a chaque article

session_start();

//on inclus toutes nos fonctions relatives a la bdd
require 'includes/bdd_functions.php';

//on spécifie le titre de notre page
$page_title = $_SESSION['username'];
//on inclus notre header et notre menu
require 'includes/header.php';

require 'includes/menu.php';

//afficher ici les infos de l'utilisateur
//possibilité de changer ses infos etc...
?>