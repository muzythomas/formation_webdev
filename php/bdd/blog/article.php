<?php
//la page article.php affiche un article et les commentaires attachés
session_start();
//on importe les fonctions relatives a la bdd
require 'includes/bdd_functions.php';

//on vérifie que le paramètre GET contenant l'id ne soit pas vide
if (!empty($_GET['id'])) {
    //on vérifie que l'id soit bien un nombre entier
    //à l'aide de la fonction php filter_var et du filtre FILTER_VALIDATE_INT
    if (filter_var($_GET['id'], FILTER_VALIDATE_INT)) {
        //si tout est ok on garde l'id
        $article_id = $_GET['id'];

        //connexion a la bdd
        try {
            $pdo = bdd_connect();
        } catch (PDOException $exception) {
            die($exception);
        }

        //récupération de l'article via bdd_getArticle
        //bdd_getArticle prend en paramètre le $pdo et l'id de l'article $article_id
        //on enregistre le résultat dans $article_data
        try {
            $article_data = bdd_getArticle($pdo, $article_id);
        } catch (Exception $exception) {
            die($exception->getMessage());
        }
        //on récupère le titre de l'article depuis le résultat de notre requête
        $article_title = $article_data['title'];

        //récupération des commentaires à l'aide de bdd_getAllComments
        //qui prend en paramètre la connexion a la bdd $pdo
        //l'id de l'article $article_id
        //et l'ordre dans lequel afficher les données DESC ou ASC
        $comments_data = bdd_getAllComments($pdo, $article_id, 'DESC');
    } else {
        //si l'id n'est pas un entier on fait planter la page
        die('ID Invalide');
    }
} else {
    //si l'id n'est pas passé en paramètre GET on fait planter la page
    die('ID Manquant');
}

//on définit le titre de notre page
$page_title = $article_title;

//on inclus le haut de page et le menu
require 'includes/header.php';

require 'includes/menu.php';
?>
    <!-- On peut commencer l'affichage de notre article -->
    <div class="article">
    <h3>
            <!-- affichage du titre et la date de notre article -->
            <?php echo htmlspecialchars($article_data['title']); ?>
            <span class="date"> - <?php echo date('d/m/y H:m', $article_data['date_sent']); ?></span>
    </h3>

    <p>
    <?php
//affichage du contenu de notre article
echo nl2br(htmlspecialchars($article_data['content']));
?>
    </p>
    </div>
    <a href="index.php">Retour</a>

    <!-- on commence l'affichage des commentaires -->
    <h3>Commentaires</h3>
    <div class="comments">
        <ul class="comment_list">

        <?php
//affichage des commentaires
foreach ($comments_data as $comment) {
    $comment_id = htmlspecialchars($comment['id']);
    $comment_author = htmlspecialchars($comment['author']);
    $comment_content = nl2br(htmlspecialchars($comment['content']));
    $comment_date = date('d/m/y', $comment['date_sent']);

    echo '<li id=' . $comment_id . '>';
    echo '<span class="com_date">Le ' . $comment_date . ' </span>';
    echo '<span class="com_author">' . $comment_author . ' a dit : </span></br>';
    echo '<span class="com_content">' . $comment_content . '</span>';
    echo '</li>';
}
?>
        <ul>
    </div>

    <form action="comment_post.php" method="post">
        <!-- on prépare un champ invisible contenant l'id de l'article sur lequel envoyer le commentaire -->
        <!-- ainsi on pourra récupérer l'id via $_POST lors du traitement du formulaire -->
        <input type="hidden" name="id_article" value="<?php echo $article_id ?>">
        <ul class="form_list">
            <fieldset>
                <legend>Commenter "<?php echo $article_title; ?>"</legend>
            <li><label for="author">Pseudo</label></li>
            <li><input placeholder="Votre pseudo" id="author" name="author" type="text" required></li>

            <li><label for="content">Votre commentaire</label></li>
            <li><textarea placeholder="Soyez courtois ! Même sur internet... :)" id="content" name="content" required></textarea></li>
            <li><button type="submit">Poster</button> </li>
            </fieldset>
        </ul>
    </form>


<?php
include 'includes/footer.php';
?>