<?php
require 'includes/bdd_functions.php';
//connexion a notre bdd
try {
    $pdo = bdd_connect();

} catch (PDOException $exception) {
    die($exception);
}

//ici nous devrions effectuer des vérifications d'intégrité de données
//les plus curieux devraient le faire ;)
$username = $_POST['username'];
$user_password = $_POST['password'];

try {
    //on récupère les données de l'user via une de nos fonctions    
    $user = bdd_getUserByUsername($pdo, $username);
} catch (Exception $exception) {
    //si une exception est lancée c'est que l'utilisateur est introuvable
    $error_message = $exception->getMessage();
    header('Location: sign_in.php?error_message=' . htmlspecialchars($error_message));
}

//si la redirection n'a pas été faite c'est que l'utilisateur a été trouvé
//une fois l'utilisateur trouvé, on peut comparer le mot de passe du formulaire
//avec le mot de passe haché contenu dans notre bdd à l'aide de password_verify()
if (password_verify($user_password, $user['password_hash'])) {
    //si password_verify renvoie true alors notre utilisateur est connecté
    session_start(); //on commence une session pour l'utilisateur
    $_SESSION['user_id'] = $user['id']; //on stocke son id
    $_SESSION['username'] = $user['username']; //on stocke son pseudo
    $_SESSION['sign_in_time'] = time(); // on stocke l'heure de sa connexion
    $_SESSION['super'] = $user['super']; //on stocke la donnée permettant d'identifier le statut utilisateur
    // (admin ou pas admin)
} else {
    $error_message = 'Mot de passe incorrect';
}
//si aucune erreur n'est survenue
if (empty($error_message)) {
    //on envoie vers l'index
    header('Location: index.php');
} else {
    //sinon on renvoie vers la page de connexion accompagnée du message d'erreur
    header('Location: sign_in.php?error_message=' . htmlspecialchars($error_message));
}
