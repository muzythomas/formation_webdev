<?php
//index.php affiche tous les articles de notre blog et les informations attachées a chaque article

session_start();

//on inclus toutes nos fonctions relatives a la bdd
require 'includes/bdd_functions.php';

//on spécifie le titre de notre page
$page_title = 'Blog de Jones';
//on inclus notre header et notre menu
require 'includes/header.php';

require 'includes/menu.php';
?>
    <h1 class="blog_title">Bienvenue sur le blog de Jones</h1>

    <?php
//connexion a la bdd à l'aide de notre fonction bdd_connect
//contenue dans le fichier includes/bdd_functions.php
try {
    $pdo = bdd_connect();
} catch (PDOException $exception) {
    die($exception);
}

//je récupère tous mes articles à l'aide de notre fonction bdd_getAllArticle
//contenue dans le fichier includes/bdd_functions.php
$data = bdd_getAllArticle($pdo, 'DESC');

//affichage de tous les articles
//pour chaque element contenu dans $data
//je fais un tour de boucle et stocke l'element dans une variable $article
foreach ($data as $article) {
    //je récupère l'id de l'article dans l'élément $article
    $article_id = $article['id'];
    // je récupère le nombre de commentaires via notre fonction bdd_countCommentsOnArticle
    $nb_comments = bdd_countCommentsOnArticle($pdo, $article_id);
    ?>
    <div class="article">
        <ul>
            <li>
                <span class="article_title">
                    <!-- je crée un lien vers la page article.php -->
                    <!-- ce lien contient un paramètre GET avec l'id unique de l'article-->
                    <a href="article.php?id=<?php echo $article_id; ?>">
                        <!-- On affiche le titre -->
                        <?php echo htmlspecialchars($article['title']); ?>
                    </a>
                    -
                    <span class="date">
                        <!-- on formate et affiche la date a partir de la timestamp -->
                        <?php echo date('d/m/y H:m', $article['date_sent']); ?>
                    </span>
                </span>
            </li>
            <li>
                <p class="article_content">
                    <?php //j'affiche le contenu de mon article
    //nl2br permet de convertir les sauts de ligne en <br>
    //pour sauter les lignes en html
    echo nl2br(htmlspecialchars($article['content']));
    ?>
                </p>
            </li>
            <li>
                <span class="article_comments">
                    <!-- Affichage du nombre de commentaires -->
                    <!-- on ne met un S à commentaire que si le nombre de commentaires dépasse 1 -->
                    <?php echo $nb_comments ?> commentaire<?php echo ($nb_comments > 1 ? 's' : '') ?> !
                    <a href="article.php?id=<?php echo $article_id; ?>"> Laissez un commentaire ! </a>
                </span>
                <!-- on passe l'id de l'article a comment.php -->
                <!-- pour savoir sur quelle article on envoie un commentaire -->
            </li>
    </ul>
    </div>

        <?php } //je ferme mon foreach?>

<?php
include 'includes/footer.php';
?>