<?php
//pour déconnecter un utilisateur
//on démarre la session
session_start();
//puis on détruit la session
session_destroy();
//puis on redirige l'utilisateur
header('Location: index.php');
?>