<?php
//bdd_functions.php regroupe les fonctions relatives a la connexion a la bdd
//et a l'execution de requêtes sur cette connexion 

/**
  * Permet de se connecter à la base de données via PDO
  * renvoie un objet PDO
  * @return PDO 
  */
function bdd_connect(){
    $host = 'localhost';
    $db = "test";
    $user = "root";
    $password = 'password';

    try {
        $pdo = new PDO('mysql:host=' . $host . ';dbname=' . $db . ';charset=utf8', $user, $password);
    
    } catch (PDOException $exception) {
        throw($exception);
    }
    return $pdo;
}

//renvoie un utilisateur récupéré via son nom d'utilisateur
//lance une exception si aucun utilisateur n'a été trouvé via ce nom
function bdd_getUserByUsername(PDO $pdo, String $username){
    $query = $pdo->prepare('SELECT id, username, password_hash FROM blog_user WHERE username = :username OR email = :username');
    $query->execute(
        array(':username' => $username)
    );
    if (!($username_data = $query->fetch())){
        throw new Exception('Utilisateur ' . $username . ' introuvable.');
    }
    $query->closeCursor();
    return $username_data;
}

/**
  * Renvoie les données d'un article grâce à son ID
  * Les données sont renvoyées dans un array
  * @param PDO $pdo le PDO permettant la connexion a la bdd
  * @param int $id l'identifiant de l'article à récupérer
  * @return array
  */
function bdd_getArticle(PDO $pdo, int $id){
    if (!filter_var($id, FILTER_VALIDATE_INT)){
        throw new Exception('id : format invalide');
    }
    $query_text = 'SELECT * FROM article WHERE id = :article_id';
    $query = $pdo->prepare($query_text);
    $query->execute(array(":article_id" => $id));   
    if (!($article_data = $query->fetch())){
        throw new Exception('Aucun article disponible avec l\'id '.$id);
    }
    $query->closeCursor();
    return $article_data;
}

//renvoie un tableau contenant tous les articles 
function bdd_getAllArticle(PDO $pdo, String $direction){
    $query_text = 'SELECT * FROM article ORDER BY date_sent ' . $direction;
    $query = $pdo->prepare($query_text);
    $query->execute();
    //fetchAll() renvoie tous les résultats dans un tableau
    //le paramètre PDO::FETCH_ASSOC permet de renvoyer un tableau associatif 
    return $query->fetchAll(PDO::FETCH_ASSOC);
}


//insère un article dans la base
//renvoie vrai ou faux selon la réussite de la requête
function bdd_createArticle(PDO $pdo, String $title, String $content, int $date_sent){
    $query = $pdo->prepare('INSERT INTO article (title, content, date_sent)
    VALUES (:title, :content, :date_sent)');
    return $query->execute(
        array(':title' => $title,
            ':content' => $content,
            ':date_sent' => $date_sent)
    ); 
}

//récupère le nombre d'articles
//renvoie un entier
function bdd_countArticles(PDO $pdo){
    $query_text = 'SELECT COUNT(*) FROM article';
    $query = $pdo->prepare($query_text);
    $query->execute();
    return $query->fetch();
}

function bdd_countCommentsOnArticle(PDO $pdo, int $id_article){
    //COUNT(*) compte le nombre de lignes dans une table 
    //si utilisé avec WHERE, elle ne compte que les lignes aux critères correspondants
    $query_text = 'SELECT COUNT(*) FROM comment WHERE id_article = :id_article';
    $query = $pdo->prepare($query_text);
    $query->execute(array(":id_article" => $id_article));
    
    //je renvoie le résultat de COUNT(*) sous la forme d'un entier 
    //cet entier représente le nombre de commentaires sur l'article
    return $query->fetchAll(PDO::FETCH_ASSOC)[0]['COUNT(*)'];
}


//récupère tous les commentaires d'un article
//renvoie un tableau contenant tous les commentaires
function bdd_getAllComments(PDO $pdo, int $id_article, String $direction){
    $query_text = 'SELECT * FROM comment WHERE id_article = :id_article ORDER BY date_sent '.$direction;
    $query = $pdo->prepare($query_text);
    $query->execute(array(":id_article" => $id_article)); 
    return $query->fetchAll(PDO::FETCH_ASSOC);
}

//insère un commentaire dans la base
//renvoie vrai ou faux selon la réussite de la requête
function bdd_createcomment(PDO $pdo, int $id_article, String $author, String $content, int $date_sent){
    $query_text = 'INSERT INTO comment (id_article, author, content, date_sent)
    VALUES (:id_article, :author, :content, :date_sent)';
    $query = $pdo->prepare($query_text);
    return $query->execute(
        array(':id_article' => $id_article,
            ':author' => $author,
            ':content' => $content,
            ':date_sent' => $date_sent)
    ); 
}



?>