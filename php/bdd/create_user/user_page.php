<?php
session_start();
if (empty($_SESSION['user_id'])){
    die('Erreur connexion');
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">

    <title>Profil de <?php echo $_SESSION['username']?></title>
</head>
<body>

    <?php echo 'Coucou ' . $_SESSION['username']; ?>
    <?php echo 'Vous êtes connecté depuis ' . date('d/m/Y H:i:s',$_SESSION['sign_in_time']);?>
    <a href="sign_out.php">Deconnexion</a>
</body>

</html>