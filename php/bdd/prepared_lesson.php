<?php
//précédemment nous avions récupéré des paramètres POST
//venant de formulaires remplis par l'utilisateur
//pour peupler nos requêtes SQL
//pour cela nous avions concanténé les paramètres directement dans notre requête
$param = $_POST['param_formulaire'];
$query_text = "SELECT " . $param . " FROM apparts";  // par exemple

//mais n'oublions pas que faire confiance à l'utilisateur est une TRES MAUVAISE IDEE
//et intégrer directement les paramètres d'un formulaire de cette façon dans une requête
//ouvre la porte a une attaque qu'on appelle attaque par injection
// https://fr.wikipedia.org/wiki/Injection_SQL 

//imaginons la requête suivante
$query_user_password = "SELECT id FROM users WHERE pseudo = 'Jones' AND password = '3916a988bb490a82b24ebc1ef9ba50ab4b7a8d2a6e95872a3cbbf11f8a392fb6' ";
//cette requête récupererait l'id de l'utilisateur Jones en vérifiant son mot de passe haché 
//cependant si on récupère le pseudo et le mot de passe en tant que paramètre
$pseudo = $_POST['pseudo'];
$password = password_hash($_POST['password']); 
$query_user_param  = "SELECT id FROM users WHERE pseudo = '". $pseudo ."'  AND password =  '". $password ."'";

//si l'utilisateur tente une injection SQL 
//il pourrait éclipser la vérification de mot de passe en renseignant le pseudo suivant
// Jones;--
// le ; terminant la requête SQL et le -- indiquant un commentaire
//ainsi la requête SQL interprétée ne sera plus 
$query_user_password = "SELECT id FROM users WHERE pseudo = 'Jones' AND password = '3916a988bb490a82b24ebc1ef9ba50ab4b7a8d2a6e95872a3cbbf11f8a392fb6' ";
//mais
$query_user_password = "SELECT id FROM users WHERE pseudo = 'Jones'; -- AND password = '3916a988bb490a82b24ebc1ef9ba50ab4b7a8d2a6e95872a3cbbf11f8a392fb6' ";
//permettant ainsi l'accès au compte de Jones sans utiliser de mot de passe, créant une énorme faille de sécurité

//on pourrait échapper les caractères spéciaux à l'aide de fonctions
//mais s'infliger ce genre de complexité supplémentaire 
// (qui ne serait en plus pas toujours infaillible et pourrait intégrer de nouvelles erreurs)
//  ne vaut clairement pas le coup compte tenue de la solution suivante

//Les Requêtes Préparées
// Liens manuel php
// http://php.net/manual/fr/pdo.prepare.php
// http://php.net/manual/fr/pdostatement.execute.php

//reprenons notre requête, en plaçant des jokers là où les valeurs doivent aller
$query_user_password = "SELECT id FROM users WHERE pseudo = ? AND password = ? ";

//pour la préparer dans notre PDO il faut faire de la façon suivante
//on crée notre connexion via l'objet PDO
$connexion = new PDO('mysql:host='.$host.';dbname='.$db.';charset=utf8', $user, $password);
//on prépare ensuite notre requête non pas avec $connexion->query() mais avec $connexion->prepare()
$query = $connexion->prepare($query_user_password); //on passe notre string contenant la requête
//puis on execute la requête en passant un tableau de paramètres contenant notre pseudo et mdp
$query->execute(array($_POST['pseudo'], $_POST['password']));

//il n'y a plus qu'à utiliser fetch() comme avant
while ($data = $query->fetch()){
    print_r($data);
}

//on peut également préparer notre requête avec des variables nominatives au lieu de jokers
$query_user_password_named = "SELECT id FROM users WHERE pseudo = :pseudo AND password = :password ";
//pour la préparer dans notre PDO il faut faire de la façon suivante
//on crée notre connexion via l'objet PDO
$connexion = new PDO('mysql:host='.$host.';dbname='.$db.';charset=utf8', $user, $password);
//on prépare ensuite notre requête non pas avec $connexion->query() mais avec $connexion->prepare()
$query = $connexion->prepare($query_user_password); //on passe notre string contenant la requête
//puis on execute la requête en passant un tableau de paramètres contenant notre pseudo et mdp
//pour les variables nominatives ce tableau devient un tableau associatif
//$query->execute(array('pseudo' => $_POST['pseudo'], 'password' => password_hash($_POST['password'])));

//on peut également (recommandé) utiliser la fonction php bindParam() avant d'utiliser execute()
$query->bindParam(':pseudo', $_POST['pseudo'], PDO::PARAM_STR); 
$query->bindParam(':password', password_hash($_POST['password']),  PDO::PARAM_STR, 32);
//la fonction bindParam (bind signifie lier) permet d'affecter une valeur a un paramètre et optionnellement spécifier son type
//la liste des types se trouve dans la documentation php  http://php.net/manual/fr/pdo.constants.php
//il ne reste plus qu'à executer notre query 
$query->execute(); // les paramètres ayant déjà été affectés, execute() se retrouve vide de paramètres

//il n'y a plus qu'à utiliser fetch() comme avant
while ($data = $query->fetch()){
    print_r($data);
}


?>

