<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Recherche apparts</title>
</head>

<body>
    <form method="post">
        <fieldset>
            <legend>Rechercher Appartement</legend>
            <fieldset>
                <legend>Filtrer le Loyer</legend>
                <label for="rent_min">Loyer mini</label>
                <input type="number" id="rent_min" name="rent_min" value="<?php echo (isset($_POST['rent_min'])) ? $_POST['rent_min'] : 0;?>">
                <label for="rent_max">Loyer maxi</label>
                <input type="number" id="rent_max" name="rent_max" value="<?php echo (isset($_POST['rent_max'])) ? $_POST['rent_max'] : 0;?>">
            </fieldset>
            <fieldset>
                <legend>Filtrer la surface</legend>
                <label for="surface_min">Surface mini</label>
                <input type="number" id="surface_min" name="surface_min" value="<?php echo (isset($_POST['surface_min'])) ? $_POST['surface_min'] : 0;?>">
                <label for="surface_max">Surface maxi</label>
                <input type="number" id="surface_max" name="surface_max" value="<?php echo (isset($_POST['surface_max'])) ? $_POST['surface_max'] : 0;?>">
            </fieldset>

            <fieldset>
                <legend for="rooms_nb">Nombre de Pièces</legend>
                <input type="checkbox" name="rooms_nb[]" value="1">1
                <input type="checkbox" name="rooms_nb[]" value="2">2
                <input type="checkbox" name="rooms_nb[]" value="3">3
                <input type="checkbox" name="rooms_nb[]" value="4">4
                <input type="checkbox" name="rooms_nb[]" value="5">5
                <input type="checkbox" name="rooms_nb[]" value="6">6
            </fieldset>
            <fieldset>
                <legend for="column">Ordonner par</legend>
                <select id="column" name="column">
                    <option value="id">Choisir</option>
                    <option value="city">Ville</option>
                    <option value="surface">Surface</option>
                    <option value="rooms">Nombre de Pièces</option>
                    <option value="rent">Loyer</option>
                </select>
            </fieldset>
            <input type="submit" value="Envoyer">
        </fieldset>
    </form>

<?php
    //on initialise les variables nous permettant de nous connecter
    //a notre base de données
    $host = 'localhost';
    $db = "test";
    $user = "root";
    $password = 'password';

    //on vérifie l'intégrité de nos variables
    //si nos variables ne sont pas initialisées ou à 0
    //on leur donne des valeurs par défaut

    //pour les checkbox la valeur par défaut est un tableau plein
    //(toutes les cases cochées)
    if (isset($_POST['rooms_nb'])){
        $checkboxes = $_POST['rooms_nb'];
    } else {
        $checkboxes = array(1,2,3,4,5,6); 
    }
    //ici on convertir notre tableau en string 
    //pour que mysql puisse le comprendre
    $rooms_nb = implode(', ', $checkboxes );

    //ici on s'assure que si la variable n'est pas initialisée
    //on lui affecte une valeur qui ne gènera pas la requête
    if (isset($_POST['rent_min']) && $_POST['rent_min'] > 0){
        $rent_min = $_POST['rent_min'];
    } else {
        $rent_min = -2147483647;
    }

    //ici on s'assure que si la variable n'est pas initialisée
    //on lui affecte une valeur qui ne gènera pas la requête
    if (isset($_POST['rent_max']) && $_POST['rent_max'] > 0){
        $rent_max = $_POST['rent_max'];
    } else {
        $rent_max = 2147483647;
    }


    //ici on s'assure que si la variable n'est pas initialisée
    //on lui affecte une valeur qui ne gènera pas la requête
    if (isset($_POST['surface_min']) && $_POST['surface_min'] > 0){
        $surface_min = $_POST['surface_min'];
    } else {
        $surface_min = -2147483647;
    }


    //ici on s'assure que si la variable n'est pas initialisée
    //on lui affecte une valeur qui ne gènera pas la requête
    if (isset($_POST['surface_max']) && $_POST['surface_max'] > 0){
        $surface_max = $_POST['surface_max'];
    } else {
        $surface_max = 2147483647;
    }


    //ici on s'assure que si la variable n'est pas initialisée
    //on lui affecte une valeur qui ne gènera pas la requête
    if (isset($_POST['column'])){
        $columns = array('id', 'city', 'surface', 'rent', 'rooms');
        $column = (in_array($_POST['column'], $columns)) ? $_POST['column'] : 'id';
    } else {
        $column = "id";
    }

    //on se connecte a la bdd
    try {
        $pdo = new PDO('mysql:host='.$host.';dbname='.$db.';charset=utf8', $user, $password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        
    } catch (PDOException $exception) {
        die($exception);
    }

    //on prépare notre requête en laissant les variables à remplacer par nos valeurs
    $query_text = 'SELECT * FROM appart WHERE (rent BETWEEN :rent_min AND :rent_max) AND (surface BETWEEN :surface_min AND :surface_max) AND rooms IN ('. $rooms_nb .') ORDER BY '.$column;

    //on envoie la requête en préparation
    $query = $pdo->prepare($query_text);

    //on lie nos paramètres à nos valeurs reçues par formulaire
    $query->bindParam(':rent_min', $rent_min, PDO::PARAM_INT);
    $query->bindParam(':rent_max', $rent_max, PDO::PARAM_INT);
    $query->bindParam(':surface_min', $surface_min, PDO::PARAM_INT);
    $query->bindParam(':surface_max', $surface_max, PDO::PARAM_INT);


    //on vérifie que execute se lance sans encombre
        if ( $query->execute()){
            //si c'est le cas on affiche le résultat
            echo '<table>';
            echo '<th>id</th>';
            echo '<th>city</th>';
            echo '<th>address</th>';
            echo '<th>zip</th>';
            echo '<th>surface</th>';
            echo '<th>rooms</th>';
            echo '<th>rent</th>';
            while ($data = $query->fetch()){ 
                echo '<tr>';
                echo '<td>'.$data['id'].'</td>';
                echo '<td>'.$data['city'].'</td>';
                echo '<td>'.$data['address'].'</td>';
                echo '<td>'.$data['zip'].'</td>';
                echo '<td>'.$data['surface'].'</td>';
                echo '<td>'.$data['rooms'].'</td>';
                echo '<td>'.$data['rent'].'</td>';
                echo '</tr>';
            }
            echo '</table>';
            $query->closeCursor();
        } else {
            //sinon on envoie un erreur
            die('Execute error');
        }

    

?>


</body>
</html>