<?php

$host = 'localhost';
$db = "test";
$user = "root";
$password = 'password';

try {
    $pdo = new PDO('mysql:host='.$host.';dbname='.$db.';charset=utf8', $user, $password);
    
} catch (PDOException $exception) {
    die($exception);
}

$query = $pdo->prepare('INSERT INTO message (id, pseudo, message)
VALUES (NULL, :pseudo, :message)');


if (isset($_POST['pseudo']) && isset($_POST['message'])){
    $pseudo = $_POST['pseudo'];
    $message = $_POST['message'];
    if (trim($pseudo) != '' && trim($message) != ''){
        $query->bindParam(':pseudo', htmlspecialchars($pseudo));
        $query->bindParam(':message', htmlspecialchars($message));
        $query->execute();
    } else {
        die("erreur: pseudo ou message vide. <a href='chat.php'>Retour</a>");
    }
} else {
    die("erreur: formulaire vide. <a href='chat.php'>Retour</a>");
}



header('Location: chat.php');
?>