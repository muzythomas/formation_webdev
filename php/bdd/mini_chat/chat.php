<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mini chat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
</head>
<body>
    <div class="container">
    <div class="message_container">
        <ul class="message_list">
        <?php
            $host = 'localhost';
            $db = "test";
            $user = "root";
            $password = 'password';
        
            
            try {
                $pdo = new PDO('mysql:host='.$host.';dbname='.$db.';charset=utf8', $user, $password);
                
            } catch (PDOException $exception) {
                die($exception);
            }
            
            $query = $pdo->prepare('SELECT * FROM message ORDER BY id ASC');
            $query->execute();
            
            while ($data = $query->fetch()){ 
                echo '<li>';
                echo '<span class="pseudo">'.$data['pseudo'].'</span>';
                echo ' : ';
                echo '<span class="m_body">'.$data['message'].'</span>';
                echo '</li>';
            }
            echo '</table>';
            $query->closeCursor();
        ?>  
        </ul>
    </div>


    <form method="post" action="chat_post.php">
        <label for="pseudo">Entrez votre Pseudo</label>
        <input required type="text" id="pseudo" name="pseudo">
        <label for="message">Tapez ici votre message :</label>
        <textarea required placeholder="Tapez ici votre message" 
        id="message" name="message" rows="5" cols="30"></textarea>
        <input type="submit" value="Envoyer">
    </form>
    </div>

    <form method="post" action="chat_post.php">
    <label for="effacer">Cocher pour effacer les messages</label>
    <input type="checkbox" name="effacer"/>
    <input type="submit" value="Confirmer"/>
    </form>
    
</body>
</html>